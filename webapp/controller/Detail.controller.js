sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/core/Element",
	"sap/ui/core/IconPool",
	"sap/m/Button",
	"sap/m/library",
	"sap/m/Text",
	"sap/m/Input",
	"sap/m/Label",
	"sap/m/Dialog"

], function (Controller, JSONModel, MessageToast, Element, IconPool, Button, mobileLibrary, Text, Input, Label, Dialog) {
	"use strict";

	var identifier;
	var dialogNumOrName;

	return Controller.extend("kis.kreditgeber.controller.Detail", {

		onInit: function () {

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Detail").attachMatched(this._onRouteMatched, this);

			this.statusSelect = new JSONModel();
			this.statusSelect.loadData(sap.ui.require.toUrl("kis/kreditgeber/model") + "/model_status.json", null, false);
			this.getView().byId("input_status").setModel(this.statusSelect);
		},
		_onRouteMatched: function (oEvent) {

			var oArgs = oEvent.getParameter("arguments");
			var oView = this.getView();

			identifier = oArgs.productId;

			oView.bindElement({
				path: "/CREDITREQUESTS(" + oArgs.productId + ")",
				events: {
					dataRequested: function () {
						oView.setBusy(true);
					},
					dataReceived: function () {
						oView.setBusy(false);
					}
				}
			});
			this.fillInputs();
			this.filterList();
		},
		fillInputs: function () {

			var oModel = this.getOwnerComponent().getModel();
			var oView = this.getView();

			oModel.read("/PERSONS", {
				filters: [
					new sap.ui.model.Filter({
						path: "PERSON_STATUS",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: 1
					}),
					new sap.ui.model.Filter({
						path: "REQUEST_ID",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: identifier
					})
				],

				success: function (oData, oResponse) {
					var n_results = oData.results.length;

					if (n_results === 1) {
						var data = oData.results[0];
						oView.byId("input_full_name").setValue(data.FULLNAME);
						oView.byId("input_email_adress").setValue(data.EMAIL_ADDRESS);
						oView.byId("input_birthday").setValue(data.BIRTHDAY);
					} else {
						console.log("Es wurde keine oder mehrere Personen gefunden");
					}
				},
				error: function (oError) {
					console.log("FEHLER: Invalid syntax");
				}
			});
			// Status des Kreditantrags aus DB lesen und in den Input schreiben
			oModel.read("/CREDITREQUESTS", {
				filters: [
					new sap.ui.model.Filter({
						path: "REQUEST_ID",
						operator: sap.ui.model.FilterOperator.EQ,
						value1: identifier
					})
				],

				success: function (oData, oResponse) {
					var n_results = oData.results.length;

					if (n_results === 1) {
						var data = oData.results[0];
						oView.byId("input_status").setSelectedKey(data.STATUS);
					} else {
						console.log("Dieser Antrag existiert nicht");
					}
				},
				error: function (oError) {
					console.log("FEHLER: Invalid syntax");
				}
			});
		},
		filterList: function () {

			var oFilter = new sap.ui.model.Filter("REQUEST_ID", sap.ui.model.FilterOperator.EQ, identifier);
			var oElement = this.getView().byId("not_imported_bank_list");
			var oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);

			oElement = this.getView().byId("imported_bank_list");
			oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);

			oElement = this.getView().byId("person_list");
			oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);

		},
		handleNavButtonPress: function (evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("TargetView1");
		},
		updateCREDITREQUESTSPress: function () {

			var oModel = this.getOwnerComponent().getModel();
			var oView = this.getView();

			//console.log(oView.byId("input_status").getSelectedKey());

			var updateCREDITREQUESTS = {
				STATUS: oView.byId("input_status").getSelectedKey()
			};

			oModel.update("/CREDITREQUESTS(" + identifier + ")", updateCREDITREQUESTS, {
				success: function (oData, oResponse) {
					MessageToast.show("Der Status des Antrags wurde aktualisiert");
				},
				error: function (oError) {
					MessageToast.show("ein Fehler ist aufgetreten");
				}
			});
		},
		notImportedBankDialogPress: function (oEvent) {

			//this.handleListItemPress(oEvent);

			var oModel = this.getOwnerComponent().getModel();
			var self = this;
			var person;

			var selectedProductREQUEST_ID = oEvent.getSource().getBindingContext().getProperty("REQUEST_ID");
			var selectedProductIBAN = oEvent.getSource().getBindingContext().getProperty("IBAN");
			var selectedProductPERSON_ID = oEvent.getSource().getBindingContext().getProperty("PERSON_ID");
			var selectedProductBANK = oEvent.getSource().getBindingContext().getProperty("BANK");
			var selectedProductSTATUS = oEvent.getSource().getBindingContext().getProperty("STATUS");

			//console.log("REQUEST_ID: " + selectedProductREQUEST_ID + "\n" + "IBAN: " + selectedProductIBAN + "\n" + "PERSON_ID: " +
			//	selectedProductPERSON_ID + "\n" + "BANK: " + selectedProductBANK + "\n" + "STATUS: " + selectedProductSTATUS);

			try {
				person = oModel.getProperty("/PERSONS(REQUEST_ID=" + selectedProductREQUEST_ID + ",PERSON_ID=" + selectedProductPERSON_ID + ")");
			} catch (e) {
				console.log("Es gibt diese Person nicht\nSollte eigentlich durch die DB Struktur nicht möglich sein");
			}

			if (!this.escapePreventDialog) {
				this.escapePreventDialog = new Dialog({

					id: "dialog",
					contentWidth: "auto",
					contentHeight: "auto",
					title: "Bankverbindung: " + selectedProductIBAN,

					content: [
						new sap.ui.layout.form.SimpleForm({
							layout: "ResponsiveGridLayout",
							content: [
								new sap.m.Label({
									text: "Dazu gehörige Person"
								}),
								new Input({
									id: "dialog_input_person",
									value: person.FULLNAME,
									editable: false
								}),
								new sap.m.Label({
									text: "Personennummer",
									required: true
								}),
								new Input({
									id: "dialog_input_personnumber",
									value: selectedProductPERSON_ID,
									placeholder: 1
								}),
								new sap.m.Label({
									text: "Namen der Bank"
								}),
								new Input({
									id: "dialog_input_bank",
									value: selectedProductBANK,
									placeholder: "Musterbank"

								}),
								new sap.m.Label({
									text: "Status"
								}),
								new Input({
									id: "dialog_input_status",
									value: selectedProductSTATUS,
									placeholder: "neu"
								}),
								new sap.m.Label({}),
								new Button({
									text: "Bankverbindung aktualisieren",
									press: function () {

										var updateIBANS = {
											PERSON_ID: sap.ui.getCore().byId("dialog_input_personnumber").getValue(),
											BANK: sap.ui.getCore().byId("dialog_input_bank").getValue(),
											STATUS: sap.ui.getCore().byId("dialog_input_status").getValue()
										};

										//console.log("/IBANS(REQUEST_ID=" + selectedProductREQUEST_ID + ",IBAN='" + selectedProductIBAN + "')");

										oModel.update("/IBANS(REQUEST_ID=" + selectedProductREQUEST_ID + ",IBAN='" + selectedProductIBAN + "')",
											updateIBANS, {
												success: function (oData, oResponse) {
													MessageToast.show("Bankverbindung wurde erfolgreich aktualisiert");
													self.escapePreventDialog.destroy();
													self.escapePreventDialog = undefined;
												},
												error: function (oError) {
													MessageToast.show("ein Fehler ist aufgetreten");
												}
											});
									}
								}),
								new Button({
									text: "sende E-Mail zum importieren",
									press: function () {
										// TODO:
										// EMAIL ADRESSE UNTER person.EMAIL_ADDRESS GESICHERT
										// nurnoch senden
										console.log(person.EMAIL_ADDRESS);
									}
								}),
								new sap.m.Label({}),
								new Button({
									visible: false
								}),
								new Button({
									text: "Bankverbindung löschen",
									type: "Negative",

									press: function () {
										oModel.remove("/IBANS(REQUEST_ID=" + selectedProductREQUEST_ID + ",IBAN='" + selectedProductIBAN + "')", {
											success: function (oData, oResponse) {
												MessageToast.show("Bankverbindung wurde erfolgreich gelöscht");

												self.escapePreventDialog.destroy();
												self.escapePreventDialog = undefined;
											},
											error: function (oError) {
												MessageToast.show("ein Fehler ist aufgetreten");
											}
										});
									}
								})
							]
						})
					],

					buttons: [
						new Button({
							text: "schließen",
							press: function () {
								this.escapePreventDialog.destroy();
								this.escapePreventDialog = undefined;
							}.bind(this)
						})
					],

					escapeHandler: function (oPromise) {
						if (!this.confirmEscapePreventDialog) {
							this.confirmEscapePreventDialog = new Dialog({

								icon: IconPool.getIconURI("message-information"),
								title: "Bist du dir sicher?",

								content: [
									new Text({
										text: "ungesicherte änderungen werden nicht gespeichert!"
									})
								],

								type: mobileLibrary.DialogType.Message,

								buttons: [
									new Button({
										text: "Ja",
										press: function () {
											this.confirmEscapePreventDialog.close();
											this.escapePreventDialog.destroy();
											// damit einer neuer dialog geöffnet werden kann muss man undefinen
											this.escapePreventDialog = undefined;
											oPromise.resolve();
										}.bind(this)
									}),
									new Button({
										text: "Nein",
										press: function () {
											this.confirmEscapePreventDialog.close();
											oPromise.reject();
										}.bind(this)
									})
								]
							});
						}
						this.confirmEscapePreventDialog.open();
					}.bind(this)
				});
			}
			this.escapePreventDialog.open();
		},
		addNotImportedBankPress: function () {

			var oModel = this.getOwnerComponent().getModel();
			var self = this;

			if (!this.escapePreventDialog) {
				this.escapePreventDialog = new Dialog({

					id: "dialog",
					contentWidth: "auto",
					contentHeight: "auto",
					title: "neue nicht importierte Banverbindung",

					content: [
						new sap.ui.layout.form.SimpleForm({
							layout: "ResponsiveGridLayout",
							content: [
								new sap.m.Label({
									text: "Antragsnummer",
									required: true
								}),
								new Input({
									id: "dialog_input_request_id",
									value: identifier,
									editable: false
								}),
								new sap.m.Label({
									text: "IBAN",
									required: true
								}),
								new Input({
									id: "dialog_input_iban"
								}),
								new sap.m.Label({
									text: "Bank"
								}),
								new Input({
									id: "dialog_input_bank"
								}),
								new sap.m.Label({
									text: "Name der Person",
									required: true
								}),
								new Input({
									id: "dialog_input_personname",
									liveChange: this.validate
								}),
								new sap.m.Label({
									text: "Personennummer",
									required: true
								}),
								new Input({
									id: "dialog_input_personnumber",
									liveChange: this.validate
								}),
								new sap.m.Label({
									text: "Status"
								}),
								new Input({
									id: "dialog_input_status"
								}),
								new sap.m.Label({}),
								new Button({
									text: "Bankverbindung anlegen",
									press: function () {

										if (dialogNumOrName === "Num") {
											console.log(dialogNumOrName);
											var createIBANS = {
												REQUEST_ID: parseInt(identifier),
												IBAN: sap.ui.getCore().byId("dialog_input_iban").getValue(),
												BANK: sap.ui.getCore().byId("dialog_input_bank").getValue(),
												PERSON_ID: parseInt(sap.ui.getCore().byId("dialog_input_personnumber").getValue()),
												STATUS: sap.ui.getCore().byId("dialog_input_status").getValue()
											};
											console.log(createIBANS);
											oModel.create("/IBANS", createIBANS, {
												success: function (oData, oResponse) {
													MessageToast.show("eine neue Bankverbindung wurde angelegt");
													self.escapePreventDialog.destroy();
													self.escapePreventDialog = undefined;
												},
												error: function (oError) {
													MessageToast.show("ein Fehler ist aufgetreten");
												}
											});
										} else {
											if (dialogNumOrName === "Name") {
												console.log(dialogNumOrName);

												oModel.read("/PERSONS", {
													filters: [
														new sap.ui.model.Filter({
															path: "REQUEST_ID",
															operator: sap.ui.model.FilterOperator.EQ,
															value1: identifier
														}),
														new sap.ui.model.Filter({
															path: "FULLNAME",
															operator: sap.ui.model.FilterOperator.EQ,
															value1: sap.ui.getCore().byId("dialog_input_personname").getValue()
														})
													],

													success: function (oData, oResponse) {
														var n_results = oData.results.length;

														if (n_results === 1) {
															var data = oData.results[0].PERSON_ID;

															var createIBANS = {
																REQUEST_ID: parseInt(identifier),
																IBAN: sap.ui.getCore().byId("dialog_input_iban").getValue(),
																BANK: sap.ui.getCore().byId("dialog_input_bank").getValue(),
																PERSON_ID: data,
																STATUS: sap.ui.getCore().byId("dialog_input_status").getValue()
															};

															oModel.create("/IBANS", createIBANS, {
																success: function (oData, oResponse) {
																	MessageToast.show("eine neue Bankverbindung wurde angelegt");
																	self.escapePreventDialog.destroy();
																	self.escapePreventDialog = undefined;
																},
																error: function (oError) {
																	MessageToast.show("ein Fehler ist aufgetreten");
																}
															});

														} else {
															console.log("Diese Person existiert nicht in diesem Antrag");
														}
													},
													error: function (oError) {
														console.log("FEHLER: Invalid syntax");
													}
												});

											} else {
												console.log("keine Person angegeben");
											}
										}

									}
								})

							]
						})
					],
					buttons: [
						new Button({
							text: "schließen",
							press: function () {
								this.escapePreventDialog.destroy();
								this.escapePreventDialog = undefined;
							}.bind(this)
						})
					],

					escapeHandler: function (oPromise) {
						if (!this.confirmEscapePreventDialog) {
							this.confirmEscapePreventDialog = new Dialog({

								icon: IconPool.getIconURI("message-information"),
								title: "Bist du dir sicher?",

								content: [
									new Text({
										text: "ungesicherte änderungen werden nicht gespeichert!"
									})
								],

								type: mobileLibrary.DialogType.Message,

								buttons: [
									new Button({
										text: "Ja",
										press: function () {
											this.confirmEscapePreventDialog.close();
											this.escapePreventDialog.destroy();
											// damit einer neuer dialog geöffnet werden kann muss man undefinen
											this.escapePreventDialog = undefined;
											oPromise.resolve();
										}.bind(this)
									}),
									new Button({
										text: "Nein",
										press: function () {
											this.confirmEscapePreventDialog.close();
											oPromise.reject();
										}.bind(this)
									})
								]
							});
						}
						this.confirmEscapePreventDialog.open();
					}.bind(this)
				});
			}
			this.escapePreventDialog.open();
		},
		validate: function () {

			//console.log("Name: " + sap.ui.getCore().byId("dialog_input_personname").getValue() + "\nNummer: " + sap.ui.getCore().byId(
			//	"dialog_input_personnumber").getValue());

			dialogNumOrName = "";

			if (sap.ui.getCore().byId("dialog_input_personname").getValue() !== "") {
				dialogNumOrName = "Name";
				sap.ui.getCore().byId("dialog_input_personnumber").setEditable(false);
			} else {
				sap.ui.getCore().byId("dialog_input_personnumber").setEditable(true);
			}
			if (sap.ui.getCore().byId("dialog_input_personnumber").getValue() !== "") {
				dialogNumOrName = "Num";
				sap.ui.getCore().byId("dialog_input_personname").setEditable(false);
			} else {
				sap.ui.getCore().byId("dialog_input_personname").setEditable(true);
			}
			//console.log(dialogNumOrName);
		},
		importedBankDialogPress: function (oEvent) {
			//this.handleListItemPress(oEvent);
		},
		personDialogPress: function (oEvent) {
			//this.handleListItemPress(oEvent);

			var oModel = this.getOwnerComponent().getModel();
			var self = this;

			var selectedProductREQUEST_ID = oEvent.getSource().getBindingContext().getProperty("REQUEST_ID");
			var selectedProductPERSON_ID = oEvent.getSource().getBindingContext().getProperty("PERSON_ID");
			var selectedProductPERSON_STATUS = oEvent.getSource().getBindingContext().getProperty("PERSON_STATUS");
			var selectedProductFULLNAME = oEvent.getSource().getBindingContext().getProperty("FULLNAME");
			var selectedProductBIRTHDAY = oEvent.getSource().getBindingContext().getProperty("BIRTHDAY");
			var selectedProductEMAIL_ADDRESS = oEvent.getSource().getBindingContext().getProperty("EMAIL_ADDRESS");

			if (!this.escapePreventDialog) {
				this.escapePreventDialog = new Dialog({

					id: "dialog",
					contentWidth: "auto",
					contentHeight: "auto",
					title: selectedProductFULLNAME,

					content: [
						new sap.ui.layout.form.SimpleForm({
							layout: "ResponsiveGridLayout",
							content: [
								new sap.m.Label({
									text: "Antragsnummer"
								}),
								new Input({
									id: "dialog_input_request_id",
									value: selectedProductREQUEST_ID,
									editable: false
								}),
								new sap.m.Label({
									text: "Personennummer"
								}),
								new Input({
									id: "dialog_input_person_id",
									value: selectedProductPERSON_ID,
									editable: false
								}),
								new sap.m.Label({
									text: "Status"
								}),
								new Input({
									id: "dialog_input_status",
									value: selectedProductPERSON_STATUS,
									editable: false
								}),
								new sap.m.Label({
									text: "Name"
								}),
								new Input({
									id: "dialog_input_person_name",
									value: selectedProductFULLNAME
								}),
								new sap.m.Label({
									text: "Geburtstag"
								}),
								new sap.m.DatePicker({
									id: "dialog_input_birthday",
									value: selectedProductBIRTHDAY
								}),
								new sap.m.Label({
									text: "E-Mail"
								}),
								new Input({
									id: "dialog_input_email_address",
									value: selectedProductEMAIL_ADDRESS
								}),
								new sap.m.Label({}),
								new Button({
									text: "Person aktualisieren",
									press: function () {

										var updatePERSONS = {
											FULLNAME: sap.ui.getCore().byId("dialog_input_person_name").getValue(),
											BIRTHDAY: sap.ui.getCore().byId("dialog_input_birthday").getDateValue(),
											EMAIL_ADDRESS: sap.ui.getCore().byId("dialog_input_email_address").getValue()
										};
										//console.log(updatePERSONS);
										oModel.update("/PERSONS(REQUEST_ID=" + selectedProductREQUEST_ID + ",PERSON_ID=" + selectedProductPERSON_ID + ")",
											updatePERSONS, {
												success: function (oData, oResponse) {
													MessageToast.show("Person wurde erfolgreich aktualisiert");
													self.escapePreventDialog.destroy();
													self.escapePreventDialog = undefined;
													self.fillInputs();

												},
												error: function (oError) {
													MessageToast.show("ein Fehler ist aufgetreten");
												}
											});
									}
								}),
								new Button({
									text: "Person zur Haupt-Kontaktperson machen",
									press: function () {
										oModel.read("/PERSONS", {
											filters: [
												new sap.ui.model.Filter({
													path: "REQUEST_ID",
													operator: sap.ui.model.FilterOperator.EQ,
													value1: identifier
												}),
												new sap.ui.model.Filter({
													path: "PERSON_STATUS",
													operator: sap.ui.model.FilterOperator.EQ,
													value1: 1
												})
											],

											success: function (oData, oResponse) {
												var n_results = oData.results.length;

												if (n_results === 1) {
													var data = oData.results[0];
													var updatePERSONS = {
														PERSON_STATUS: 0
													};
													oModel.update("/PERSONS(REQUEST_ID=" + data.REQUEST_ID + ",PERSON_ID=" + data.PERSON_ID + ")", updatePERSONS, {
														success: function (oData, oResponse) {
															MessageToast.show("Der Status des Antrags wurde aktualisiert");
															sap.ui.getCore().byId("dialog_input_status").setValue(updatePERSONS.PERSON_STATUS);
														},
														error: function (oError) {
															MessageToast.show("ein Fehler ist aufgetreten");
														}
													});
													var updatePERSONS = {
														PERSON_STATUS: 1
													};
													oModel.update("/PERSONS(REQUEST_ID=" + selectedProductREQUEST_ID + ",PERSON_ID=" + selectedProductPERSON_ID +
														")", updatePERSONS, {
															success: function (oData, oResponse) {
																MessageToast.show("Der Status des Antrags wurde aktualisiert");
																sap.ui.getCore().byId("dialog_input_status").setValue(updatePERSONS.PERSON_STATUS);
																self.fillInputs();
															},
															error: function (oError) {
																MessageToast.show("ein Fehler ist aufgetreten");
															}
														});

												} else {
													console.log("Dieser Antrag existiert nicht");
												}
											},
											error: function (oError) {
												console.log("FEHLER: Invalid syntax");
											}
										});
									}
								}),
								new sap.m.Label({}),
								new Button({
									visible: false
								}),
								new Button({
									text: "Person löschen",
									type: "Negative",

									press: function () {
										if (selectedProductPERSON_STATUS === 1) {
											console.log("hello");
										} else {
											oModel.remove("/PERSONS(REQUEST_ID=" + selectedProductREQUEST_ID + ",PERSON_ID=" + selectedProductPERSON_ID + ")", {
												success: function (oData, oResponse) {
													MessageToast.show("Person wurde erfolgreich gelöscht");

													self.escapePreventDialog.destroy();
													self.escapePreventDialog = undefined;
												},
												error: function (oError) {
													MessageToast.show("ein Fehler ist aufgetreten");
												}
											});
										}
									}
								})
							]
						})
					],

					buttons: [
						new Button({
							text: "schließen",
							press: function () {
								this.escapePreventDialog.destroy();
								this.escapePreventDialog = undefined;
							}.bind(this)
						})
					],

					escapeHandler: function (oPromise) {
						if (!this.confirmEscapePreventDialog) {
							this.confirmEscapePreventDialog = new Dialog({

								icon: IconPool.getIconURI("message-information"),
								title: "Bist du dir sicher?",

								content: [
									new Text({
										text: "ungesicherte änderungen werden nicht gespeichert!"
									})
								],

								type: mobileLibrary.DialogType.Message,

								buttons: [
									new Button({
										text: "Ja",
										press: function () {
											this.confirmEscapePreventDialog.close();
											this.escapePreventDialog.destroy();
											// damit einer neuer dialog geöffnet werden kann muss man undefinen
											this.escapePreventDialog = undefined;
											oPromise.resolve();
										}.bind(this)
									}),
									new Button({
										text: "Nein",
										press: function () {
											this.confirmEscapePreventDialog.close();
											oPromise.reject();
										}.bind(this)
									})
								]
							});
						}
						this.confirmEscapePreventDialog.open();
					}.bind(this)
				});
			}
			this.escapePreventDialog.open();
		},
		addPersonPress: function () {

			var oModel = this.getOwnerComponent().getModel();
			var self = this;
			var max = this.getView().byId("person_list").getItems().length + 1;

			if (!this.escapePreventDialog) {
				this.escapePreventDialog = new Dialog({

					id: "dialog",
					contentWidth: "auto",
					contentHeight: "auto",
					title: "neue Person Banverbindung",

					content: [
						new sap.ui.layout.form.SimpleForm({
							layout: "ResponsiveGridLayout",
							content: [
								new sap.m.Label({
									text: "Antragsnummer",
									required: true
								}),
								new Input({
									id: "dialog_input_request_id",
									value: identifier,
									editable: false
								}),
								new sap.m.Label({
									text: "Personennummer",
									required: true
								}),
								new Input({
									id: "dialog_input_person_id",
									value: max,
									editable: false
								}),
								new sap.m.Label({
									text: "Status",
									required: true
								}),
								new Input({
									id: "dialog_input_status",
									value: 0,
									editable: false
								}),
								new sap.m.Label({
									text: "Name der Person",
									required: true
								}),
								new Input({
									id: "dialog_input_personname"
								}),
								new sap.m.Label({
									text: "Geburtstag",
									required: true
								}),
								new sap.m.DatePicker({
									id: "dialog_input_birthday"
								}),
								new sap.m.Label({
									text: "E-Mail Adresse",
									required: true
								}),
								new Input({
									id: "dialog_input_email_address"
								}),
								new sap.m.Label({}),
								new Button({
									text: "Person hinzufügen",
									press: function () {

										var createPERSONS = {
											REQUEST_ID: parseInt(identifier),
											PERSON_ID: sap.ui.getCore().byId("dialog_input_person_id").getValue(),
											PERSON_STATUS: sap.ui.getCore().byId("dialog_input_status").getValue(),
											FULLNAME: sap.ui.getCore().byId("dialog_input_personname").getValue(),
											BIRTHDAY: sap.ui.getCore().byId("dialog_input_birthday").getDateValue(),
											EMAIL_ADDRESS: sap.ui.getCore().byId("dialog_input_email_address").getValue()
										};
										console.log(createPERSONS);
										oModel.create("/PERSONS", createPERSONS, {
											success: function (oData, oResponse) {
												MessageToast.show("eine neue Person wurde hinzugefügt");
												self.escapePreventDialog.destroy();
												self.escapePreventDialog = undefined;
											},
											error: function (oError) {
												MessageToast.show("ein Fehler ist aufgetreten");
											}
										});

									}
								})

							]
						})
					],
					buttons: [
						new Button({
							text: "schließen",
							press: function () {
								this.escapePreventDialog.destroy();
								this.escapePreventDialog = undefined;
							}.bind(this)
						})
					],

					escapeHandler: function (oPromise) {
						if (!this.confirmEscapePreventDialog) {
							this.confirmEscapePreventDialog = new Dialog({

								icon: IconPool.getIconURI("message-information"),
								title: "Bist du dir sicher?",

								content: [
									new Text({
										text: "ungesicherte änderungen werden nicht gespeichert!"
									})
								],

								type: mobileLibrary.DialogType.Message,

								buttons: [
									new Button({
										text: "Ja",
										press: function () {
											this.confirmEscapePreventDialog.close();
											this.escapePreventDialog.destroy();
											// damit einer neuer dialog geöffnet werden kann muss man undefinen
											this.escapePreventDialog = undefined;
											oPromise.resolve();
										}.bind(this)
									}),
									new Button({
										text: "Nein",
										press: function () {
											this.confirmEscapePreventDialog.close();
											oPromise.reject();
										}.bind(this)
									})
								]
							});
						}
						this.confirmEscapePreventDialog.open();
					}.bind(this)
				});
			}
			this.escapePreventDialog.open();
		},
		handleListItemPress: function (oEvent) {
			console.log("You pressed item: " + oEvent.getSource().getBindingContext());

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var selectedREQUEST_ID = oEvent.getSource().getBindingContext().getProperty("REQUEST_ID");
			var selectedBANKCONNECTION_ID = oEvent.getSource().getBindingContext().getProperty("BANKCONNECTION_ID");
			oRouter.navTo("Bank", {
				requestId: selectedREQUEST_ID,
				bankConnectionId: selectedBANKCONNECTION_ID
			});
		}
	});
});