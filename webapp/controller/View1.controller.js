sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast"
], function (Controller, JSONModel, MessageToast) {
	"use strict";

	/* was fehlt :
	
			PERSONEN
				View1
							- archiv 
							- person email (NURY)
							- wenn man bei Bankverbindugen oder Personen auf löschen klickt wird der Inhalt alle felder gelöscht

				Detail
							- Antrag löschen
							- email senden
								HAPUTPERSON KANN NICHT GELÖSCHT WERDEN - KANN SCHNELL GEÄNDERT WERDEN
									- SOLL NUR GELÖSCHT WERDEN WENN AUCH DER ANTRAG GELÖSCHT WIRD
							
				Transactions
							- Table verbessern
			*/

	var numberPerson = 1;
	var numberBankconnection = 0;

	//sap.ui.getCore() - für den Controller
	//this.getView()	- für die View

	return Controller.extend("kis.kreditgeber.controller.View1", {
		onInit: function () {
			// side_navigation aus Model laden
			this.SideNavigation = new JSONModel();
			this.SideNavigation.loadData(sap.ui.require.toUrl("kis/kreditgeber/model") + "/model_side_navigation.json", null, false);
			this.getView().byId("side_navigation").setModel(this.SideNavigation);
			// antrag_erstellen zahlen laaden
			this.getView().byId("inputPersonenanzahl").setValue(numberPerson);
			this.getView().byId("inputBankverbindungAnzahl").setValue(numberBankconnection);
			this.onBankPlusPress();

		},
		// side_navigation welche Seite laden ?
		onItemSelect: function (oEvent) {
			var item = oEvent.getParameter("item");
			this.byId("pageContainer").to(this.getView().createId(item.getKey()));
			this.filterList();
		},
		// side_navigation ein und ausklappen
		onMenuButtonPress: function () {
			var toolPage = this.byId("toolPage");
			toolPage.setSideExpanded(!toolPage.getSideExpanded());

		},
		filterListHelper: function (what, status, list) {
			var oFilter = new sap.ui.model.Filter(what, sap.ui.model.FilterOperator.EQ, status);
			var oElement = this.getView().byId(list);
			var oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);
		},
		// Listen filtern für "fertig","fehler" und "in Bearbeitung"
		filterList: function () {
			this.filterListHelper("STATUS", "Information", "new");
			this.filterListHelper("STATUS", "Error", "error");
			this.filterListHelper("STATUS", "Success", "success");
			//			this.getView().byId("archiv").getBinding("items").refresh(true);
		},
		// Routing für einzelne Anträge
		handleListItemPress: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var selectedProductId = oEvent.getSource().getBindingContext().getProperty("REQUEST_ID");
			oRouter.navTo("Detail", {
				productId: selectedProductId
			});

			console.log("You pressed item: " + oEvent.getSource().getBindingContext());

		},
		// Antrag erstellen
		onPersonPlusPress: function () {
			numberPerson++;
			this.getView().byId("inputPersonenanzahl").setValue(numberPerson);
			this.getView().byId("test").addContent(
				new sap.m.Label({

				})
			);
			this.getView().byId("test").addContent(
				new sap.m.Text({
					text: "Personnummer: " + numberPerson
				})
			);
			this.getView().byId("test").addContent(
				new sap.m.Label({
					text: "Name"
				})
			);
			//neuen input erstellen
			this.getView().byId("test").addContent(
				new sap.m.Input({
					id: "person" + numberPerson,
					placeholder: "person" + numberPerson
						//placeholder: "Max Mustermann"
				})
			);
			this.getView().byId("test").addContent(
				new sap.m.Label({
					text: "E-Mail"
				})
			);
			this.getView().byId("test").addContent(
				new sap.m.Input({
					id: "mail" + numberPerson,
					placeholder: "mail" + numberPerson
						//placeholder: "Max Mustermann"
				})
			);
			this.getView().byId("test").addContent(
				new sap.m.Label({
					text: "Geburtsdatum"
				})
			);
			this.getView().byId("test").addContent(
				new sap.m.DatePicker({
					id: "birthday" + numberPerson,
					placeholder: "birthday" + numberPerson
						//placeholder: "Max Mustermann"
				})
			);

		},
		onPersonMinusPress: function () {

			if (numberPerson > 1) {
				numberPerson--;
				this.getView().byId("inputPersonenanzahl").setValue(numberPerson);
				this.getView().byId("test").destroyContent();
				for (var i = 2; i <= numberPerson; i++) {
					// neues Label
					this.getView().byId("test").addContent(
						new sap.m.Label({

						})
					);
					this.getView().byId("test").addContent(
						new sap.m.Text({
							text: "Personnummer: " + i
						})
					);
					this.getView().byId("test").addContent(
						new sap.m.Label({
							text: "Name"
						})
					);
					// neuer Input
					this.getView().byId("test").addContent(
						new sap.m.Input({
							id: "person" + i,
							placeholder: "person" + i
								//placeholder: "DE01234567890123456789"
						})
					);
					this.getView().byId("test").addContent(
						new sap.m.Label({
							text: "E-Mail"
						})
					);
					this.getView().byId("test").addContent(
						new sap.m.Input({
							id: "mail" + i,
							placeholder: "mail" + i
								//placeholder: "Max Mustermann"
						})
					);
					this.getView().byId("test").addContent(
						new sap.m.Label({
							text: "Geburtsdatum"
						})
					);
					this.getView().byId("test").addContent(
						new sap.m.DatePicker({
							id: "birthday" + i,
							placeholder: "birthday" + i
								//placeholder: "Max Mustermann"
						})
					);
				}
			}

		},
		onBankPlusPress: function () {

			numberBankconnection++;
			this.getView().byId("inputBankverbindungAnzahl").setValue(numberBankconnection);
			this.getView().byId("Bankverbindungen").addContent(
				new sap.m.Label({
					text: "Bankverbindung " + numberBankconnection
				})
			);
			//neuen input erstellen
			this.getView().byId("Bankverbindungen").addContent(
				new sap.m.Input({
					id: "bank_input_" + numberBankconnection,
					placeholder: "bank_input_" + numberBankconnection
						//placeholder: "DE01234567890123456789"
				})
			);
			this.getView().byId("Bankverbindungen").addContent(
				new sap.m.Label({
					text: "Nummer der verknüpften Person "
				})
			);
			//neuen input erstellen
			this.getView().byId("Bankverbindungen").addContent(
				new sap.m.Input({
					id: "bank_person_input_" + numberBankconnection,
					placeholder: "1"
						//placeholder: "DE01234567890123456789"
				})
			);

		},
		onBankMinusPress: function () {
			if (numberBankconnection > 1) {
				numberBankconnection--;
				this.getView().byId("inputBankverbindungAnzahl").setValue(numberBankconnection);
				this.getView().byId("Bankverbindungen").destroyContent();

				for (var i = 1; i <= numberBankconnection; i++) {

					this.getView().byId("Bankverbindungen").addContent(
						new sap.m.Label({
							text: "Bankverbindung " + i
						})
					);
					this.getView().byId("Bankverbindungen").addContent(
						new sap.m.Input({
							id: "bank_input_" + i,
							placeholder: "bank_input_" + i
								//placeholder: "DE01234567890123456789"
						})
					);
					this.getView().byId("Bankverbindungen").addContent(
						new sap.m.Label({
							text: "Nummer der verknüpften Person "
						})
					);
					//neuen input erstellen
					this.getView().byId("Bankverbindungen").addContent(
						new sap.m.Input({
							id: "bank_person_input_" + i,
							placeholder: "1"
								//placeholder: "DE01234567890123456789"
						})
					);

				}

			}
		},
		createDatabaseEntry: function (where, what) {
			var oModel = this.getOwnerComponent().getModel();
			oModel.create(where, what, {
				success: function (oData, oResponse) {
					MessageToast.show("Antrag wurde erfolgreich erstellt");
				},
				error: function (oError) {
					MessageToast.show("ein Fehler ist aufgetreten");
				}
			});
		},
		onSavePress: function () {
			var newCreditRequestEntry = {
				// Daten für neuen Datenbankeintrag aus den Inputfeldern holen
				//	FULL_NAME: this.getView().byId("inputAntragssteller").getValue(),
				//	EMAIL_ADRESS: this.getView().byId("inputE-Mail").getValue(),
				//	BIRTHDAY: this.getView().byId("inputDatum").getValue(),
				REQUEST_ID: parseInt(this.getView().byId("inputKreditantragsnummer").getValue()),
				USERNAME: this.getView().byId("inputBenutzername").getValue(),
				STATUS: "Information"
			};
			console.log(newCreditRequestEntry);

			// Database call
			this.createDatabaseEntry("/CREDITREQUESTS", newCreditRequestEntry);
			var newMainPersonEntry = {
				REQUEST_ID: newCreditRequestEntry.REQUEST_ID,
				PERSON_ID: 1,
				PERSON_STATUS: 1,
				FULLNAME: this.getView().byId("inputAntragssteller").getValue(),
				BIRTHDAY: this.getView().byId("inputDatum").getDateValue(),
				EMAIL_ADDRESS: this.getView().byId("inputE-Mail").getValue()
			};
			console.log(newMainPersonEntry);
			this.createDatabaseEntry("/PERSONS", newMainPersonEntry);

			// Personen speichern
			for (var j = 2; j <= numberPerson; j++) {
				var newPersonEntry = {
					REQUEST_ID: newCreditRequestEntry.REQUEST_ID,
					PERSON_ID: parseInt(j),
					PERSON_STATUS: 0,
					FULLNAME: sap.ui.getCore().byId("person" + j).getValue(),
					BIRTHDAY: sap.ui.getCore().byId("birthday" + j).getDateValue(),
					EMAIL_ADDRESS: sap.ui.getCore().byId("mail" + j).getValue()
				};
				console.log(newPersonEntry);
				this.createDatabaseEntry("/PERSONS", newPersonEntry);
			}

			// Bankverbindungen speichern
			for (var i = 1; i <= numberBankconnection; i++) {
				var newBankconnectionEntry = {
					REQUEST_ID: newCreditRequestEntry.REQUEST_ID,
					IBAN: sap.ui.getCore().byId("bank_input_" + i).getValue(),
					PERSON_ID: sap.ui.getCore().byId("bank_person_input_" + i).getValue(),
					STATUS: "new"
				};

				console.log(newBankconnectionEntry);
				this.createDatabaseEntry("/IBANS", newBankconnectionEntry);
			}

			/*
									//send Email
									var data = {
										"email": newCreditRequestEntry.EMAIL_ADRESS,
										"id": newCreditRequestEntry.REQUEST_ID
									};
									//POST request to xsjs service  Response format " {success: boolean}"
									$.ajax({
										type: "POST",
										url: '/destinations/finAPI/sendMailOnCreation.xsjs',
										data: JSON.stringify(data),
										contentType: 'application/json',
										dataType: "json",
										success: function () { //nicht notwendig
											console.log("succ");
										},
										error: function () {
											console.log("fail");
										}
									});
			*/
			//	this.filterList();
			//	this.onDeletePress();
		},
		onDeletePress: function () {
			this.getView().byId("inputAntragssteller").setValue("");
			this.getView().byId("inputE-Mail").setValue("");
			this.getView().byId("inputDatum").setValue("");
			this.getView().byId("inputKreditantragsnummer").setValue("");
			this.getView().byId("inputBenutzername").setValue("");
			for (var j = 2; j <= numberPerson; j++) {
				sap.ui.getCore().byId("person" + j).setValue("");
				sap.ui.getCore().byId("mail" + j).setValue("");
				sap.ui.getCore().byId("birthday" + j).setValue("");
			}
			while (numberPerson > 1) {
				this.onPersonMinusPress();
			}
			for (var i = 1; i <= numberBankconnection; i++) {
				sap.ui.getCore().byId("bank_input_" + i).setValue("");
			}
			while (numberBankconnection > 1) {
				this.onBankMinusPress();
			}
		}
	});
});