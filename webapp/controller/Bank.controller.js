sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	var navBackRequestId, navBackBankConnectionId;
	return Controller.extend("kis.kreditgeber.controller.Bank", {

		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Bank").attachMatched(this._onRouteMatched, this);

		},
		_onRouteMatched: function (oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			navBackRequestId = oArgs.requestId;
			navBackBankConnectionId = oArgs.bankConnectionId;
			oView = this.getView();
			oView.bindElement({
				path: "/ACCOUNTS",
				events: {
					dataRequested: function () {
						oView.setBusy(true);
					},
					dataReceived: function () {
						oView.setBusy(false);
					}
				}
			});
			this.filterList();
		},
		filterList: function () {

			var oFilter = new sap.ui.model.Filter([
				new sap.ui.model.Filter("REQUEST_ID", sap.ui.model.FilterOperator.EQ, navBackRequestId),
				new sap.ui.model.Filter("BANKCONNECTION_ID", sap.ui.model.FilterOperator.EQ, navBackBankConnectionId)
			], true);

			var oElement = this.getView().byId("imported_bank_list");
			var oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);

		},
		handleNavButtonPress: function (evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Detail", {
				productId: navBackRequestId
			});
		},
		handleListItemPress: function (oEvent) {
			console.log("You pressed item: " + oEvent.getSource().getBindingContext());

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var selectedREQUEST_ID = oEvent.getSource().getBindingContext().getProperty("REQUEST_ID");
			var selectedBANKCONNECTION_ID = oEvent.getSource().getBindingContext().getProperty("BANKCONNECTION_ID");
			var selectedACCOUNT_ID = oEvent.getSource().getBindingContext().getProperty("ACCOUNT_ID");
			oRouter.navTo("Transactions", {
				requestId: selectedREQUEST_ID,
				bankConnectionId: selectedBANKCONNECTION_ID,
				accountId: selectedACCOUNT_ID
			});
		}
	});

});