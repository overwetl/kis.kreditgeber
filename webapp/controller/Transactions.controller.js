sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	var navBackRequestId, navBackBankConnectionId, navBackAccountId;

	return Controller.extend("kis.kreditgeber.controller.Transactions", {

		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("Transactions").attachMatched(this._onRouteMatched, this);

		},
		_onRouteMatched: function (oEvent) {
			var oArgs, oView;
			oArgs = oEvent.getParameter("arguments");
			navBackRequestId = oArgs.requestId;
			navBackBankConnectionId = oArgs.bankConnectionId;
			navBackAccountId = oArgs.accountId;
			oView = this.getView();
			oView.bindElement({
				path: "/TRANSACTIONS",
				events: {
					dataRequested: function () {
						oView.setBusy(true);
					},
					dataReceived: function () {
						oView.setBusy(false);
					}
				}
			});
			this.filterList();
		},
		filterList: function () {

			var oFilter = new sap.ui.model.Filter([
				new sap.ui.model.Filter("REQUEST_ID", sap.ui.model.FilterOperator.EQ, navBackRequestId),
				new sap.ui.model.Filter("BANKCONNECTION_ID", sap.ui.model.FilterOperator.EQ, navBackBankConnectionId),
				new sap.ui.model.Filter("ACCOUNT_ID", sap.ui.model.FilterOperator.EQ, navBackAccountId)
			], true);

			var oElement = this.getView().byId("idProductsTable");
			var oBinding = oElement.getBinding("items");
			oBinding.filter([oFilter]);

		},
		handleNavButtonPress: function (evt) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Bank", {
				requestId: navBackRequestId,
				bankConnectionId: navBackBankConnectionId
			});
		}

	});

});