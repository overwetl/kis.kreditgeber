CREATE TRIGGER "KIS"."archiv_trigger" 
AFTER delete ON "KIS"."CREDIT_REQUEST"
    REFERENCING OLD ROW myold    
    FOR EACH ROW                                             
    BEGIN                                                   
        INSERT INTO "CREDIT_ARCHIV" values (:myold.REQUEST_ID,:myold.STATUS,:myold.FULL_NAME,:myold.USERNAME,:myold.EMAIL_ADRESS,:myold.BIRTHDAY,:myold.PASSWORD_ACC);
    END
